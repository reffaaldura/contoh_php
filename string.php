<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h2>Berlatih String PHP</h2>
    <?php
        echo "<h3> Soal No 1</h3>";
        $kalimat1 = "Heloo PHP!" ;
        echo "First_sentence : " . $kalimat1 . "<br>";
        echo "Panjang string : " . strlen($kalimat1)."<br>";
        echo " Jumlah Kata : " . str_word_count($kalimat1)."<br><br>";

        $kalimat2 = "I'm ready for the challenges" ;
        echo "Second_sentence : " . $kalimat2 . "<br>";
        echo "Panjang string : " . strlen($kalimat2)."<br>";
        echo " Jumlah Kata : " . str_word_count($kalimat2)."<br><br>";

        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP" ;
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: ".substr($string2, 2, 4)  ;
        echo "<br> Kata Ketiga: " .substr($string2, 7, 3) ;

        echo "<h3> Soal No 3</h3>";
        $string3 = "PHP is old but Good!";
        echo "kalimat ketiga : ". $string3."<br>"; 
        echo "Ganti kalimat ketiga :". str_replace ("Good","awesome",$string3);
        ?>
</body>
</html>